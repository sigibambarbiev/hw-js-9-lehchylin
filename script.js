
// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//    Создать новый HTML тег на странице можно с помощью document.createElement(tag) - создается новый элемент с заданным тэгом.
//    document.createTextNode(text) - создается новый текстовый узел с заданным текстом.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//    insertAdjacentHTML - это способ вставить в элемент строку как HTML, т.е. строку с учетом HTML-разметки. Первый параметр указывает куда вставить строку, "beforebegin" - вставить непосредственно перед элементом;
//    "afterbegin" -вставить в начало элемента;
//    "beforeend" - вставиить в конец элемента;
//    "afterend" - вставить непосредственно после элементом;

// 3. Як можна видалити елемент зі сторінки?
//    Удалить элемент можно с помощью метода node.remove()

const arrayOriginal = ["hello", "world", "Kiev", ["Borispol", "Irpen"], "Kharkiv", "Odessa", ["Yuzniy", "Chernomorsk", "Izmail"], "Lviv"];

let createLiElem;
let createLiElemNested;
let createUlElem;

function arrayList(array, parentElement) {

    const createParentElement = document.createElement(parentElement);
    document.body.prepend(createParentElement);

    array.forEach(elem => {

        if (!!Array.isArray(elem)) {
            let newArray = elem.map(number => number);

            createUlElem = document.createElement('ul');
            createLiElem.append(createUlElem);

            newArray.forEach(elem => {

                createLiElemNested = document.createElement('li');
                createLiElemNested.innerText = elem;
                createUlElem.append(createLiElemNested);

            })
            return;
        }

        createLiElem = document.createElement('li');
        createLiElem.innerText = elem;
        createParentElement.append(createLiElem);
    })
}

arrayList(arrayOriginal, 'ul');


function timeOut() {
    document.body.innerHTML = ''
}

setTimeout(timeOut, 10000);





